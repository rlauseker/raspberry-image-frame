#!/bin/bash
# wait for docker mqtt to be there on startup
sleep 15s

MQTTLAUNCHERCONFIG=/root/mqtt-launcher/launcher.conf python /root/mqtt-launcher/mqtt-launcher.py &