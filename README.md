# Raspberry Image Frame

## Idea

Usability and privacy are the primary thoughts behind this project. 
Most commercial image frames only support fixed storage or external access via an proprietary service/app. Open source project like [Dynaframe](https://github.com/Geektoolkit/Dynaframe) and [OpenFrame](https://github.com/OpenFrameProject) exist, but are either complex to setup self hosted, or lack features as-is.

## Prerequisites 

### Hardware

* Raspberry Pi 3/4
* [RPI HC-SR501](http://www.netzmafia.de/skripten/hardware/RasPi/Projekt-PIR/index.html)
* HDMI Display
* HDD (Backups, optional)

### Software Stack

* Docker
    * node-red
        * node-red-node-pi-gpiod
            * [pigpiod](http://abyz.me.uk/rpi/pigpio/pigpiod.html)
        * node-red-contrib-telegrambot
    * mqtt
    * next-cloud (optional)
* Docker Compose
* [mqtt-launcher](https://github.com/jpmens/mqtt-launcher)
    * python
    * pip
        * paho-mqtt
* feh

### Accounts / Services

* [Telegram Bot Token](https://core.telegram.org/bots)

## Setup

`apt install unattended-upgrades git logrotate docker.io docker-compose python3 pigpiod feh`

## ToDos

* Auto-inject node-red setup on install.
* Single docker compose file
* Secure setup
    * mqtt / pigpiod only on internal IPs
* Look into and implement alternatives to Telegram to inject images. (mail, signal, XMPP, etc.)
* Look into alternatives to display images and add video support. ([Dynaframe](https://github.com/Geektoolkit/Dynaframe))