docker run -d \
        --log-opt max-size=10m \
        --log-opt max-file=5 \
        -p 1883:1883 \
        -p 9001:9001 \
        -p 1884:1884 \
        --network mqtt
        --restart always \
        -v /srv/mqtt/config:/mosquitto/config \
        -v /srv/mqtt/log:/mosquitto/log \
        -v /src/mqtt/data:/mosquitto/data \
        --name mqtt \
        eclipse-mosquitto