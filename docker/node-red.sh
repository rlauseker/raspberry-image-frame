docker run -d \
        --log-opt max-size=10m \
        --log-opt max-file=5 \
        --restart always \
        -p 1880:1880 \
        --network mqtt
        -v /srv/docker/node-red:/data \
        --name node-red \
        nodered/node-red:latest